package bra2cloud.main;

public abstract class ExperimentDefinitions {
	
	public static final String KEY_EXPERIMENT_REGIONS = "experiment.regions";
	public static final String KEY_EXPERIMENT_ZONES = "experiment.zones";
	public static final String KEY_EXPERIMENT_INSTANCES = "experiment.instances";

}