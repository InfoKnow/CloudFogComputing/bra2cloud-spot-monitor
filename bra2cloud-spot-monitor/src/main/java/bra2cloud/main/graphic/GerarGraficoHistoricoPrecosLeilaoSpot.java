package bra2cloud.main.graphic;
import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jfree.data.time.RegularTimePeriod;
import org.jfree.data.time.Second;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;

import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.model.AvailabilityZone;

import bra2cloud.cloud.aws.core.AWSManager;
import bra2cloud.cloud.aws.dao.SpotDAO;
import bra2cloud.cloud.aws.util.AWSUtil;
import bra2cloud.core.util.PropertiesUtil;

public class GerarGraficoHistoricoPrecosLeilaoSpot extends AbstractBaseTask {
	
	private static final Logger log = LogManager.getLogger(GerarGraficoHistoricoPrecosLeilaoSpot.class);
	
	private SpotDAO dao;
	
	public GerarGraficoHistoricoPrecosLeilaoSpot() {
		dao = new SpotDAO();
	}

	public void run() throws SQLException, IOException {
		
		
		List<String> regions = PropertiesUtil.getInstance().getList(KEY_EXPERIMENT_REGIONS);

		boolean allInstances = PropertiesUtil.getInstance().getProperty(KEY_EXPERIMENT_INSTANCES).equalsIgnoreCase("all");
		
		List<String> instances = allInstances ? AWSUtil.allInstances : PropertiesUtil.getInstance().getList(KEY_EXPERIMENT_INSTANCES);

		
		for (String region : regions) {
			
			AmazonEC2 ec2Exec = AWSManager.getInstance().getAmazonEC2(region);
			
			List<AvailabilityZone> availabilityZones = ec2Exec.describeAvailabilityZones().getAvailabilityZones();
			
			log.info("Zones to be processed in "+ region +": "+ availabilityZones.stream().map(AvailabilityZone::getZoneName).collect(Collectors.toList()));
			
			for (AvailabilityZone zone : availabilityZones) {
				
				for (String instance : instances) {
					TimeSeriesCollection dataset = new TimeSeriesCollection();
					TimeSeries series = new TimeSeries("Prices");
					
					Calendar calInit = Calendar.getInstance();
					calInit.add(Calendar.MONTH, -2);
					calInit.set(Calendar.DAY_OF_MONTH, 1);
					
					Calendar calEnd = Calendar.getInstance();
					calEnd.set(Calendar.DAY_OF_MONTH, 1);
					calEnd.add(Calendar.DAY_OF_MONTH, -1);
					
					Map<Long, Double> map = dao.findPriceChangeHistory(region, zone.getZoneName(), instance, calInit.getTime(), calEnd.getTime());
					
					for (Map.Entry<Long, Double> entry : map.entrySet()) {
						Calendar cal = Calendar.getInstance();
						cal.setTimeInMillis(entry.getKey());
//					RegularTimePeriod timePeriod = new Day(cal.getTime());
//					RegularTimePeriod timePeriod = new Minute(cal.getTime());
						RegularTimePeriod timePeriod = new Second(cal.getTime());
						series.addOrUpdate(timePeriod, entry.getValue());
					}
					
					dataset.addSeries(series);
					
					if (series.getItemCount() > 0) {
						
						SimpleDateFormat sdf = new SimpleDateFormat("YYYYMMdd");
						
						String filename = "prices_"+ region + "_"+ zone.getZoneName() +"_"+ sdf.format(calInit.getTime()) +"-"+ sdf.format(calEnd.getTime()) +"_"+ instance;
						String title = "Price changes "+ zone.getZoneName() +"/"+ instance +" "+ sdf.format(calInit.getTime()) +" to "+ sdf.format(calEnd.getTime()) +" ("+ series.getItemCount() +")";
						super.exportImageTimeSeriesChart(dataset, filename, title, "Date", "Price");
						
					}
					
					
				}
				
				log.info("Done.");
				
			}
			
		}
		
		

	}

	public static void main(String[] args) throws SQLException, IOException {
		new GerarGraficoHistoricoPrecosLeilaoSpot().run();
	}

}
