package bra2cloud.core.db;

import java.io.File;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.postgresql.copy.CopyManager;
import org.postgresql.core.BaseConnection;

import com.amazonaws.util.StringUtils;

import bra2cloud.core.exceptions.TooManyConnectionsException;
import bra2cloud.core.util.GlobalCount;
import bra2cloud.core.util.PropertiesUtil;

public class PostgresDBConnection {
	
	private static final Logger log = LogManager.getLogger(PostgresDBConnection.class);
	
	private static PostgresDBConnection uniqueInstance = null;
	private Connection connection;
	
	
	private PostgresDBConnection() {
//		this.connection = createConnection();
	}
	
	private Connection createConnection() {
		
		Connection newConnection = null;
		
		PropertiesUtil prop = PropertiesUtil.getInstance();
		String host = prop.getProperty("pg.host");
		String port = prop.getProperty("pg.port");
		String db = prop.getProperty("pg.database");
		String user = prop.getProperty("pg.user");
		String pass = prop.getProperty("pg.password");
		
		String databaseUrl = "jdbc:postgresql://"+ host +":"+ port +"/"+ db;
		
		try {
			
			String simpleDatabaseUrl = host +":"+ port +"/"+ db;
			log.info("Trying to connect to database "+ simpleDatabaseUrl);
			
			Class.forName("org.postgresql.Driver").newInstance();
			newConnection = DriverManager.getConnection(databaseUrl, user, pass);
			newConnection.setAutoCommit(false);
			log.info("Connected to "+ simpleDatabaseUrl +".");
			
		} catch (SQLException | InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			log.error("Error to connect database: "+ e.getMessage());
			System.exit(-1);
		}
		
		return newConnection;
	}
	
	public static void closeConnection(Connection con) throws SQLException {
		if (con != null) {
			con.close();
		}
	}
	
	public static PostgresDBConnection getInstance() {
		if (uniqueInstance == null) {
			uniqueInstance = new PostgresDBConnection();
		}
		return uniqueInstance;
	}
	
	public Connection getConnection() {
		return connection;
	}
	
	public Connection getNewConnection() throws TooManyConnectionsException {
		return createConnection();
	}
	
	public void copyFromCSV(String table, File[] files) throws Exception {
		
		if (StringUtils.isNullOrEmpty(table) || (files != null && files.length == 0)) {
			throw new IllegalArgumentException("Table and files are required to import CSV files to PostgreSQL! ");
		}
		
		Connection con = getNewConnection();
		
		Statement stmt = con.createStatement();
		stmt.executeUpdate("TRUNCATE "+ table);
		stmt.close();
		con.commit();
		
		try {
			closeConnection(con);
		} catch (SQLException e) {
			log.error("Error when disconnect database in in TRUNCATE command: "+ e.getMessage());
		}

		
		int poolSize = Runtime.getRuntime().availableProcessors() * 2;
		ExecutorService executor = Executors.newFixedThreadPool(poolSize);
		
		String countKey = "importCasesToPostgresThreadCount";
		
		for (File csvFile : files) {
			
			Runnable r = new Runnable() {
				@Override
				public void run() {
					Connection newConnection = getNewConnection();
					try {
						CopyManager copyManager = new CopyManager((BaseConnection) newConnection);
						
						log.info("Importing file "+ csvFile.getAbsolutePath());
						FileReader fileReader = new FileReader(csvFile);
						String copyCommand = "COPY "+ table +" FROM STDIN WITH DELIMITER ';' CSV HEADER;";
						long rows = copyManager.copyIn(copyCommand, fileReader);
						log.info("Rows in "+ csvFile.getName() +": "+ rows);
						newConnection.commit();
						GlobalCount.reduceCount(countKey);
					} catch (Exception e) {
						log.error("Error when importing "+ csvFile +" to postgres: "+ e.getMessage());
					} finally {
						try {
							closeConnection(newConnection);
						} catch (SQLException e) {
							log.error("Error when disconnect database in "+ csvFile +" to postgres: "+ e.getMessage());
						}
					}
				}
			};
			executor.execute(r);
			GlobalCount.addCount(countKey);
			
		}
		
		executor.shutdown();
        while (!executor.isTerminated()) {
        	Thread.sleep(5000);
			log.info("Waiting...");
        }
        log.info("Done importing "+ files.length +" files.");
	}
	
}